/** This function initializes the currency selction lists by fetching from the local json file
 * @author Raagav Prasanna
 */
function initialiazelists() {
    let myFile = "../json/currency.json";
    let baseList = document.querySelector('#base_currency_select');
    let toList = document.querySelector('#to_currency_select');
    fetch(myFile)
        .then(response => response.json())
        .then(data => {
            for (let curr of Object.entries(data)) {
                allObjArr.push(curr[1]);
                let option1 = document.createElement("option");
                option1.setAttribute('value', curr[1].name);
                let option2 = document.createElement("option");
                option2.setAttribute('value', curr[1].name);

                baseList.appendChild(option1);
                toList.appendChild(option2);
                option1.innerHTML = curr[1].name;
                option2.innerHTML = curr[1].name;
            }
        })
        .catch(error => {
            alert(error);
        });
}

/** This function initializes the url that will be later used to fetch from the api. It then calls the fetchConversion method from inside it.
 * @author Raagav Prasanna
 */
function initializeUrl() {
    let my_btn = document.querySelector('#get_data_btn');

    my_btn.addEventListener('click', () => {
        let currencyInitial = document.querySelector('#base_currency_select').value;
        let currencyFinal = document.querySelector('#to_currency_select').value;
        let conversionAmt = document.querySelector('#amount').value;
        let codeInitial = 'initial';
        let codeFinal = 'final';
        let initUrl = 'https://api.exchangerate.host/convert';
        allObjArr.forEach(element => {
            if (element.name == currencyInitial) {
                codeInitial = element.code;
            }
            if (element.name == currencyFinal) {
                codeFinal = element.code;
            }
        });
        let absoluteUrl = initUrl + `?from=${codeInitial}&to=${codeFinal}&amount=${conversionAmt}`;
        fetchConversion(absoluteUrl);
    });
}

/** This function fetches the json object from the api, by taking input a string representing the url. It then calls the appendTable function.
 * @author Raagav Prasanna
 * @param {String} inpUrl - The url that will be used to fetch from the api 
 */
function fetchConversion(inpUrl) {
    fetch(inpUrl)
        .then(response => response.json())
        .then(data => {
            apppendTable(data);
        })
        .catch(error => {
            alert(error);
        });
}

/** This function takes as input a json object and then appends the data found in the object to the table
 * 
 * @param {Object} curObj - represents the data that is fetched from the api
 * @author Raagav Prasanna 
 */
function apppendTable(curObj) {
    let cur_tab = document.querySelector('#data-table');
    let cur_date = new Date() + "";
    let inpRow = [curObj.query.from, curObj.query.to, curObj.info.rate, curObj.query.amount,
    curObj.result, cur_date.substring(0, 25)];
    let row = document.createElement('tr');
    cur_tab.appendChild(row);

    inpRow.forEach(element => {
        let inpField = document.createElement('td');
        row.appendChild(inpField);
        inpField.innerHTML = element;
    });
}

